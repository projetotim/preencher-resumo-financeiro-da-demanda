SET SERVEROUTPUT ON SIZE UNLIMITED;

DECLARE
    l_portfolio_destino_name   VARCHAR(255);
    l_portfolio_destino_id     NUMBER;
    l_count_test               NUMBER;
        
    CURSOR c1 IS
    SELECT DISTINCT
        fs.financial_summary_id,
        r.request_id,
        lpe.active_entity,
        fs.name
    FROM
        pfm_lifecycle_parent_entity   lpe
        JOIN kcrt_requests                 r ON r.request_id = decode(lpe.active_entity, 'Proposal', lpe.proposal_req_id, 'Project', lpe.project_req_id
        ,
                                                      'Asset', lpe.asset_req_id)
        JOIN fm_financial_summary          fs ON lpe.lifecycle_id = fs.parent_id
                                        AND fs.parent_entity_id = 526
                                        AND fs.entity_type = 'FS'
    WHERE
        r.request_id IN (
            );

BEGIN
    FOR c1_rec IN c1 LOOP
        IF c1_rec.active_entity = 'Project' THEN
            UPDATE kcrt_fg_pfm_project
            SET
                prj_financial_summary_id = c1_rec.financial_summary_id,
                prj_financial_summary_name = c1_rec.name
            WHERE
                request_id = c1_rec.request_id;

        ELSIF c1_rec.active_entity = 'Proposal' THEN
            UPDATE kcrt_fg_pfm_proposal
            SET
                prop_financial_summary_id = c1_rec.financial_summary_id,
                prop_financial_summary_name = c1_rec.name
            WHERE
                request_id = c1_rec.request_id;

        END IF;

        UPDATE kcrt_requests
        SET
            last_update_date = sysdate,
            last_updated_by = 1
        WHERE
            request_id = c1_rec.request_id;

        UPDATE kcrt_request_details
        SET
            last_update_date = sysdate,
            last_updated_by = 1
        WHERE
            request_id = c1_rec.request_id;

        UPDATE kcrt_req_header_details
        SET
            last_update_date = sysdate,
            last_updated_by = 1
        WHERE
            request_id = c1_rec.request_id;

    END LOOP;
END;
/